package com.challenge.gist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.challenge.gist.R;
import com.challenge.gist.constant.ApiConstants;
import com.challenge.gist.databinding.GistListItemBinding;
import com.challenge.gist.model.Gist;
import com.challenge.gist.model.GistFile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class GistRVAdapter extends RecyclerView.Adapter<GistRVAdapter.GistViewHolder> {
    private Context context;
    private ArrayList<Gist> list;
    private GistListItemBinding binding;
    private final OnItemClickListener listener;

    public GistRVAdapter(Context context, ArrayList<Gist> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public GistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = GistListItemBinding.inflate(inflater,parent,false);
        return new GistViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GistViewHolder holder, int position) {
        Gist gist = list.get(position);

        Glide.with(context).load(gist.getOwner().getAvatarUrl())
                .into(holder.itemBinding.imvAvatar);
        holder.itemBinding.tvLogin.setText(gist.getOwner().getLogin());
        Map<String, GistFile> files = gist.getFiles();
        if(files != null && files.size() > 0) {
            holder.itemBinding.btnFilename.setText(files.entrySet().iterator().next().getKey());
        }else{
            holder.itemBinding.btnFilename.setText(R.string.no_description);
        }
        holder.itemBinding.tvComments.setText(context.getString(R.string.num_comments, gist.getComments()));

        try {
            Date newDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).parse(gist.getCreatedAt());
            if(newDate != null) {
                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US).format(newDate);
                holder.itemBinding.tvCreationDate.setText(date);
            }else{
                holder.itemBinding.tvCreationDate.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(gist.getDescription() != null  && !gist.getDescription().isEmpty()) {
            holder.itemBinding.tvDescription.setText(gist.getDescription());
        }else{
            holder.itemBinding.tvDescription.setVisibility(View.GONE);
        }
        holder.bind(gist, listener);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class GistViewHolder extends RecyclerView.ViewHolder{
        private GistListItemBinding itemBinding;

        public GistViewHolder(GistListItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }

        public void bind(final Gist gist, final OnItemClickListener listener) {
            itemBinding.btnFilename.setOnClickListener(v -> {
                listener.onItemClick(gist);
            });
        }
    }

    public  void resetList(ArrayList<Gist> updatedList){
        list = updatedList;
        notifyDataSetChanged();
    }

    public void updateList(ArrayList<Gist> updatedList){
        int currentSize = list.size();
        list.addAll(updatedList);
        notifyItemRangeInserted(currentSize, ApiConstants.GISTS_PER_PAGE);
    }

    public interface OnItemClickListener {
        void onItemClick(Gist item);
    }
}