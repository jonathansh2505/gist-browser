package com.challenge.gist.viewmodel;

import android.util.Log;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.challenge.gist.repository.Repository;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class GistFileDetailViewModel extends ViewModel {

    private static final String TAG = "DVModel";

    private Repository repository;
    private MutableLiveData<String> fileDetail = new MutableLiveData<>();

    @ViewModelInject
    public GistFileDetailViewModel(Repository repository) {
        this.repository = repository;
    }

    public MutableLiveData<String> getFileDetail() {
        return fileDetail;
    }

    public void getFileContent(String fileUrl){
        repository.getFileContent(fileUrl)
                .subscribeOn(Schedulers.io())
                .map(new Function<ResponseBody, ResponseBody>() {
                    @Override
                    public ResponseBody apply(ResponseBody fileContent) throws Throwable {
                        fileDetail.postValue(fileContent.string());
                        return fileContent;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            Log.d(TAG, String.format("File Loaded %s", fileDetail.getValue()));
                        },
                        error-> Log.e(TAG, String.format("Loading File Content Error -> %s", error.getMessage())));
    }
}
