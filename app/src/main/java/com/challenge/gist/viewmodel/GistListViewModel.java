package com.challenge.gist.viewmodel;

import android.util.Log;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.challenge.gist.model.Gist;
import com.challenge.gist.repository.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class GistListViewModel extends ViewModel {

    private static final String TAG = "DVModel";

    private Disposable disposable;
    private Repository repository;
    private MutableLiveData<ArrayList<Gist>> gistList = new MutableLiveData<>();
    private int currentPage = 0;

    @ViewModelInject
    public GistListViewModel(Repository repository) {
        this.repository = repository;
        initDisposable();
    }

    private void initDisposable(){
        disposable = Observable.interval(0, 15,
                TimeUnit.MINUTES)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::getGists, this::onError);
    }

    private void onError(Throwable throwable) {
        Log.d(TAG, "Failed Loading Gists");
    }

    public MutableLiveData<ArrayList<Gist>> getGistList() {
        return gistList;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void getGists(Long aLong){
        Log.d(TAG, String.format("Loading Gists Refresh -> %d", aLong));
        getGistsPerPage(0);
    }

    public void getGistsPerPage(int page){
        this.currentPage = page;
        repository.getGistList(currentPage)
                .subscribeOn(Schedulers.io())
                .map(new Function<List<Gist>, ArrayList<Gist>>() {
                    @Override
                    public ArrayList<Gist> apply(List<Gist> gistResponse) {
                        return (ArrayList)gistResponse;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> gistList.setValue(result),
                        error-> Log.e(TAG, String.format("Loading Gists Error -> %s", error.getMessage())));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.dispose();
    }

    public void onResume() {
        if (disposable.isDisposed()) {
            initDisposable();
        }
    }

    public void onPause(){
        disposable.dispose();
    }
}
