package com.challenge.gist.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GistFile implements Serializable {

    private String filename;

    private String type;

    @SerializedName("raw_url")
    private String rawUrl;



    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRawUrl() {
        return rawUrl;
    }

    public void setRawUrl(String rawUrl) {
        this.rawUrl = rawUrl;
    }
}
