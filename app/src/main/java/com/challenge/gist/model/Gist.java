package com.challenge.gist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedHashMap;

public class Gist implements Serializable {

    private String id;
    private Owner owner;
    @SerializedName("files")
    @Expose
    private LinkedHashMap<String, GistFile> files;
    private String description;
    @SerializedName("created_at")
    private String createdAt;
    private int comments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public LinkedHashMap<String, GistFile> getFiles() {
        return files;
    }

    public void setFiles(LinkedHashMap<String, GistFile> files) {
        this.files = files;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
