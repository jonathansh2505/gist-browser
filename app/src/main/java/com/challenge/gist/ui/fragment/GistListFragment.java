package com.challenge.gist.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.challenge.gist.adapter.GistRVAdapter;
import com.challenge.gist.databinding.FragmentGistListBinding;
import com.challenge.gist.listener.EndlessScrollListener;
import com.challenge.gist.model.Gist;
import com.challenge.gist.model.GistFile;
import com.challenge.gist.viewmodel.GistListViewModel;

import java.util.ArrayList;
import java.util.Map;

import dagger.hilt.android.AndroidEntryPoint;


@AndroidEntryPoint
public class GistListFragment extends Fragment implements GistRVAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "GistFragment";
    private FragmentGistListBinding binding;
    private GistListViewModel viewModel;
    private GistRVAdapter adapter;
    private ArrayList<Gist> gistList;
    EndlessScrollListener endlessScrollListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentGistListBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(GistListViewModel.class);

        initRecyclerView();
        observeData();
        //loadGists();
    }

    private void loadGists(int page){
        binding.srlGist.setRefreshing(true);
        viewModel.getGistsPerPage(page);
    }

    private void observeData() {
        viewModel.getGistList().observe(getViewLifecycleOwner(), gists -> {
            Log.e(TAG, "onChanged: " + gists.size());
            binding.srlGist.setRefreshing(false);
            if(viewModel.getCurrentPage() == 0) {
                endlessScrollListener.resetPreviousTotal();
                adapter.resetList(gists);
            }else{
                adapter.updateList(gists);
            }
        });
    }

    private void initRecyclerView() {
        LinearLayoutManager llManager = new LinearLayoutManager(getContext());
        binding.rvGist.setLayoutManager(llManager);
        adapter = new GistRVAdapter(getContext(),gistList, this);
        binding.rvGist.setAdapter(adapter);
        endlessScrollListener = new EndlessScrollListener(llManager) {
            @Override
            public void onLoadMore(int current_page) {
                if(current_page > 0){
                    current_page -= 1;
                }
                loadGists(current_page);
            }
        };
        binding.rvGist.addOnScrollListener(endlessScrollListener);
        binding.srlGist .setOnRefreshListener(this);
        binding.srlGist.setRefreshing(true);
    }

    @Override
    public void onItemClick(Gist gist) {
        Map<String, GistFile> files = gist.getFiles();
        String filename = "";
        String url = "";
        if(files != null && files.size() > 0) {
            GistFile file = files.entrySet().iterator().next().getValue();
            filename = file.getFilename();
            url = file.getRawUrl();
        }
        GistListFragmentDirections.ActionListGistFragmentToGistFileDetailFragment action =
                GistListFragmentDirections.actionListGistFragmentToGistFileDetailFragment();
        action.setFilename(filename);
        action.setRawUrl(url);
        Navigation.findNavController(getView()).navigate(action);
    }

    @Override
    public void onRefresh() {
        loadGists(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        viewModel.onPause();
    }
}