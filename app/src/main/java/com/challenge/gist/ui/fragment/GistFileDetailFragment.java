package com.challenge.gist.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.challenge.gist.databinding.FragmentGistFileDetailBinding;
import com.challenge.gist.viewmodel.GistFileDetailViewModel;

import dagger.hilt.android.AndroidEntryPoint;


@AndroidEntryPoint
public class GistFileDetailFragment extends Fragment{

    private FragmentGistFileDetailBinding binding;
    private GistFileDetailViewModel viewModel;
    private GistFileDetailFragmentArgs args;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentGistFileDetailBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(GistFileDetailViewModel.class);
        args = GistFileDetailFragmentArgs.fromBundle(getArguments());
        observeData();
        loadFile();
    }

    private void loadFile(){
        viewModel.getFileContent(args.getRawUrl());
    }

    private void observeData() {
        viewModel.getFileDetail().observe(getViewLifecycleOwner(), content -> {
            binding.tvContent.setText(content);
        });
    }

}