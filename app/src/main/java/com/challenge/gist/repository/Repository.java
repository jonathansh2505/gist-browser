package com.challenge.gist.repository;

import com.challenge.gist.constant.ApiConstants;
import com.challenge.gist.model.Gist;
import com.challenge.gist.network.NetworkApiService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Observable;
import okhttp3.ResponseBody;

public class Repository {

    private NetworkApiService apiService;

    @Inject
    public Repository(NetworkApiService apiService){
        this.apiService = apiService;
    }


    public Observable<List<Gist>> getGistList(int page){
        return apiService.getGistList(page, ApiConstants.GISTS_PER_PAGE);
    }

    public Observable<ResponseBody> getFileContent(String fileUrl){
        return apiService.getFileContent(fileUrl);
    }

}
