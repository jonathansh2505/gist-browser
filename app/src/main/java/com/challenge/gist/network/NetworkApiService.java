package com.challenge.gist.network;

import com.challenge.gist.constant.ApiConstants;
import com.challenge.gist.model.Gist;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface NetworkApiService {
    @GET(ApiConstants.LIST_GIST_URL)
    Observable<List<Gist>> getGistList(@Query("page") int page, @Query("per_page") int pageSize);

    @Streaming
    @GET
    Observable<ResponseBody> getFileContent(@Url String fileUrl);
}
