package com.challenge.gist.constant;

public interface ApiConstants {

    String BASE_URL = "https://api.github.com";

    String LIST_GIST_URL = "gists/public";

    int GISTS_PER_PAGE = 100;

}
