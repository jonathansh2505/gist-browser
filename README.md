# README #

This is a small implementation of Gists Browser

### What this contains? ###

* List of Gists with pagination on Scroll down, Swipe to refresh and auto refresh every 15 minutes
* Detail of the contents of the first Gist file

### Whats Next? ###

* Add Room for local storage
* Improve pagination after implementing Room
* UI Improvements
